module "certificate_authority" {
  source = "git::https://gitlab.com/enchantments/certificate_authority.git"

  organization = "Antifascist Security Group"
  province     = "Duwamish Territory"
  locality     = "Seattle"
}
