module "vault_initialization" {
  source = "git::https://gitlab.com/enchantments/vault_initialization.git"

  control_plane_ips             = "${module.control_plane.public_ipv4_addresses}"
  control_plane_instance_names  = "${module.control_plane.instance_names}"
  ssh_user                      = var.ssh_user
  ssh_private_key               = var.ssh_private_key
}
