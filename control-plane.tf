module "control_plane" {
  source = "git::https://gitlab.com/enchantments/control_plane.git"

  region     = "asgard"
  datacenter = "gce-${var.zone}"
  instances  = 3

  ssh_user                     = var.ssh_user
  ssh_private_key              = var.ssh_private_key
  project                      = var.project
  zone                         = var.zone
  
  intermediate_private_key_pem = module.certificate_authority.intermediate_private_key_pem
  intermediate_certificate_pem = module.certificate_authority.intermediate_certificate_pem
  root_certificate_pem         = module.certificate_authority.root_certificate_pem
  organization                 = module.certificate_authority.organization
  province                     = module.certificate_authority.province
  locality                     = module.certificate_authority.locality
}
